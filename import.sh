#!/usr/bin/env bash

# install script to create the database
mysql -u root -p$MYSQL_ROOT_PASSWORD < /tmp/create_db_table_schema.sql

# change the database settings
mysql -u root -p$MYSQL_ROOT_PASSWORD < /tmp/db_settings.sql

