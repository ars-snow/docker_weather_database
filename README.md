# Weather Database in docker

Create the docker containers, one for the Python code and one for the database
`docker-compose up -d`

If this is the first time setting up the containers then we have to create the database tables
`docker exec -it mysql_weather bash
sh /tmp/import.sh
exit`

Build the WeatherDatabase wxdb image
`docker build -t wxdb .`

Test that the config file generates the metadata into the database
docker run -it -v /Users/scott/Documents/Projects/WeatherDatabase/:/code --link mysql_weather:mysql --net weatherdatabase_default wxdb python wxdb/w
xdb.py scripts/config_metadata.ini

docker-compose run wxdb python wxdb/wxdb.py scripts/config_metadata.ini
