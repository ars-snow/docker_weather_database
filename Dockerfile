
FROM python:3.5

MAINTAINER Scott Havens <scott.havens@ars.usda.gov>

RUN mkdir /code
WORKDIR /code

# install the requirements which is seperate from the projects
ADD requirements.txt /code/
RUN pip install -r requirements.txt

# TODO: create an entry script here that can take a 'metadata' or 'data' and config argument
# to run the whole thing 
CMD [ "python"]